package com.gitlab.uwa9k073.chat.config;

import com.auth0.jwt.exceptions.JWTVerificationException;
import com.gitlab.uwa9k073.chat.exceptions.InvalidClaimsException;
import com.gitlab.uwa9k073.chat.exceptions.InvalidTokenException;
import com.gitlab.uwa9k073.chat.exceptions.TokenExpiresException;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

@Slf4j
@AllArgsConstructor
@Component
public class JwtAuthFilter extends OncePerRequestFilter {

  private final UserAuthenticationProvider userAuthenticationProvider;

  private static final String BEARER_PREFIX = "Bearer ";
  private final static Integer TOKEN_START_INDEX = 7;

  @Override
  protected void doFilterInternal(
      HttpServletRequest request,
      HttpServletResponse response,
      FilterChain filterChain) throws ServletException, IOException {
    String header = request.getHeader(HttpHeaders.AUTHORIZATION);

    if (header == null || !header.startsWith(BEARER_PREFIX)) {
      response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
      return;
    }
    var token = header.substring(TOKEN_START_INDEX);
    try {
      var context = SecurityContextHolder.createEmptyContext();
      var payload = userAuthenticationProvider.validateToken(token);
      context.setAuthentication(
          new UsernamePasswordAuthenticationToken(payload, null, null));
      SecurityContextHolder.setContext(context);
      filterChain.doFilter(request, response);
    } catch (InvalidClaimsException | InvalidTokenException | TokenExpiresException
             | JWTVerificationException e) {
      log.warn(e.getMessage());
      response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
    }
  }
}


