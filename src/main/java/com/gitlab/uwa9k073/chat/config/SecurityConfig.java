package com.gitlab.uwa9k073.chat.config;

import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

@AllArgsConstructor
@Configuration
@EnableWebSecurity
public class SecurityConfig {

  private final JwtAuthFilter jwtAuthFilter;

  @Bean
  public SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity) throws Exception {
    httpSecurity
        .cors(AbstractHttpConfigurer::disable).csrf(AbstractHttpConfigurer::disable)
        .sessionManagement(customizer -> customizer.sessionCreationPolicy(
            SessionCreationPolicy.STATELESS))
        .authorizeHttpRequests(
            requests -> requests.requestMatchers("/v1/chat", "/v1/chat/fill",
                    "/v1/chat/{chatSessionId}", "/v1/chat/{chatSessionId}/messages",
                    "/v1/chat/{chatSessionId}/messages/fill",
                    "/v1/chat/{chatSessionId}/messages/{messageId}").authenticated().anyRequest()
                .permitAll())
        .addFilterBefore(jwtAuthFilter, BasicAuthenticationFilter.class);

    return httpSecurity.build();
  }
}
