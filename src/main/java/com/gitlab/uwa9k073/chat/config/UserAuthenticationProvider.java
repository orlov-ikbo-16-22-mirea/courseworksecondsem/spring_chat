package com.gitlab.uwa9k073.chat.config;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.gitlab.uwa9k073.chat.dtos.TokenDto;
import com.gitlab.uwa9k073.chat.dtos.TokenPayloadDto;
import com.gitlab.uwa9k073.chat.exceptions.InvalidClaimsException;
import com.gitlab.uwa9k073.chat.exceptions.TokenExpiresException;
import jakarta.annotation.PostConstruct;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Base64;
import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
@Slf4j
public class UserAuthenticationProvider {

  private static final List<String> EXPECTED_CLAIMS = List.of("exp", "userId", "sessionId");
  @Value("${service-secrets.access-token.secret-key}")
  private String accessTokenSecretKey;
  @Value("${service-secrets.access-token.ttl.amount}")
  private Long accessTokenTtlAmount;
  @Value("${service-secrets.access-token.ttl.chrono-unit}")
  private String accessTokenTtlChronoUnit;
  private Algorithm accessTokenAlg;

  @PostConstruct
  protected void init() {
    accessTokenSecretKey = Base64.getEncoder().encodeToString(accessTokenSecretKey.getBytes());
    accessTokenAlg = Algorithm.HMAC512(accessTokenSecretKey);
  }

  private String signToken(UUID userId, UUID sessionId) {
    Algorithm current = accessTokenAlg;
    Instant expiresAt = Instant.now()
        .plus(accessTokenTtlAmount, ChronoUnit.valueOf(accessTokenTtlChronoUnit.toUpperCase()));
    return JWT.create()
        .withExpiresAt(expiresAt)
        .withClaim(EXPECTED_CLAIMS.get(1), userId.toString())
        .withClaim(EXPECTED_CLAIMS.get(2), sessionId.toString())
        .sign(current);
  }

  public TokenDto createToken(UUID userId, UUID sessionId) {
    return TokenDto.builder().token(signToken(userId, sessionId)).build();
  }

  public TokenPayloadDto validateToken(String token)
      throws IllegalArgumentException {
    var decodedToken = JWT.decode(token);
    accessTokenAlg.verify(decodedToken);

    var claims = decodedToken.getClaims();
    for (var claim : EXPECTED_CLAIMS) {
      if (!claims.containsKey(claim)) {
        throw new InvalidClaimsException(String.format("Claim %s not found", claim));
      }
    }
    var expAt = Instant.ofEpochSecond(claims.get(EXPECTED_CLAIMS.getFirst()).as(Long.class), 1);
    if (Instant.now().isAfter(expAt)) {
      throw new TokenExpiresException("Token is expires");
    }
    log.debug(EXPECTED_CLAIMS.get(1));
    log.debug(EXPECTED_CLAIMS.get(2));
    return TokenPayloadDto.builder()
        .userId(UUID.fromString(claims.get(EXPECTED_CLAIMS.get(1)).asString()))
        .sessionId(UUID.fromString(claims.get(EXPECTED_CLAIMS.get(2)).asString())).build();
  }
}
