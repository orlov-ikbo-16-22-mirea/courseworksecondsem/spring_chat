package com.gitlab.uwa9k073.chat.controllers;


import com.gitlab.uwa9k073.chat.dtos.ChatSessionDto;
import com.gitlab.uwa9k073.chat.dtos.TokenPayloadDto;
import com.gitlab.uwa9k073.chat.exceptions.AppException;
import com.gitlab.uwa9k073.chat.exceptions.ResourceNotFound;
import com.gitlab.uwa9k073.chat.exceptions.UniqueViolation;
import com.gitlab.uwa9k073.chat.services.ChatSessionsService;
import java.net.URI;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/chat")
@AllArgsConstructor
@Slf4j
public class ChatSessionsController {

  private ChatSessionsService service;

  @PostMapping
  ResponseEntity<?> createChat(@RequestHeader(name = "X-Idempotency-Key") UUID idempotencyKey,
      @RequestBody ChatSessionDto chatSessionDto) {
    try {
      var session = service.create(chatSessionDto, idempotencyKey);
      return ResponseEntity.created(
          URI.create(String.format("/v1/chat/%s", session.getId().toString()))).body(session);
    } catch (UniqueViolation e) {
      log.warn(e.getMessage());
      return ResponseEntity.ok().build();
    }
  }

  @GetMapping("/fill")
  ResponseEntity<?> getUserChatSessions() {
    try {
      var userId = ((TokenPayloadDto) SecurityContextHolder.getContext().getAuthentication()
          .getPrincipal()).getUserId();
      var sessions = service.getAll(userId);
      return ResponseEntity.ok(sessions);
    } catch (ResourceNotFound e) {
      log.warn(e.getMessage());
      throw new AppException(e.getMessage(), HttpStatus.NOT_FOUND);
    }
  }

  @GetMapping("/{chatSessionId}")
  ResponseEntity<?> getChatSession(@PathVariable UUID chatSessionId) {
    try {
      log.debug("Start processing getting chat by id");
      log.debug(String.format("CHAT ID: %s", chatSessionId.toString()));
      var session = service.getById(chatSessionId);
      return ResponseEntity.ok(session);
    } catch (ResourceNotFound e) {
      log.warn(e.getMessage());
      throw new AppException(e.getMessage(), HttpStatus.NOT_FOUND);
    }
  }

  @DeleteMapping("/{chatSessionId}")
  ResponseEntity<?> deleteChatSession(@PathVariable UUID chatSessionId) {
    try {
      service.deleteByChatId(chatSessionId);
      return ResponseEntity.noContent().build();
    } catch (ResourceNotFound e) {
      log.warn(e.getMessage());
      throw new AppException(e.getMessage(), HttpStatus.NOT_FOUND);
    }
  }
}
