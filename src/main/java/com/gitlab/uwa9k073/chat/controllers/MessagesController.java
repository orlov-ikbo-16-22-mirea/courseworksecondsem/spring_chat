package com.gitlab.uwa9k073.chat.controllers;


import com.gitlab.uwa9k073.chat.dtos.MessageDto;
import com.gitlab.uwa9k073.chat.dtos.TokenPayloadDto;
import com.gitlab.uwa9k073.chat.exceptions.AppException;
import com.gitlab.uwa9k073.chat.exceptions.ConflictException;
import com.gitlab.uwa9k073.chat.exceptions.ResourceNotFound;
import com.gitlab.uwa9k073.chat.exceptions.UniqueViolation;
import com.gitlab.uwa9k073.chat.services.MessagesService;
import jakarta.servlet.http.HttpServletRequest;
import java.net.URI;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/v1/chat/{chatSessionId}/messages")
@AllArgsConstructor
public class MessagesController {

  private MessagesService service;

  @GetMapping("/fill")
  public ResponseEntity<?> getAllMessages(HttpServletRequest request,
      @PathVariable UUID chatSessionId) {
    try {
      var messages = service.getAllInChat(chatSessionId);
      return ResponseEntity.ok(messages);
    } catch (ResourceNotFound e) {
      log.warn(e.getMessage());
      throw new AppException(e.getMessage(), HttpStatus.NOT_FOUND);
    }
  }

  @DeleteMapping("/{messageId}")
  public ResponseEntity<?> deleteMessage(HttpServletRequest request,
      @PathVariable UUID chatSessionId, @PathVariable UUID messageId) {
    try {
      var userId = ((TokenPayloadDto) SecurityContextHolder.getContext().getAuthentication()
          .getPrincipal()).getUserId();
      service.deleteMessage(chatSessionId, userId, messageId);

      return ResponseEntity.noContent().build();
    } catch (ResourceNotFound e) {
      log.warn(e.getMessage());
      throw new AppException(e.getMessage(), HttpStatus.NOT_FOUND);
    }
  }

  @PostMapping
  public ResponseEntity<?> createMessage(HttpServletRequest request,
      @RequestBody MessageDto messageDto,
      @RequestHeader(name = "X-Idempotency-Key") UUID idempotencyKey,
      @PathVariable UUID chatSessionId) {
    try {
      var userId = ((TokenPayloadDto) SecurityContextHolder.getContext().getAuthentication()
          .getPrincipal()).getUserId();
      if (!messageDto.getChatId().equals(chatSessionId)) {
        log.debug("AWS CONTROLLER DEBUG");
        log.debug(messageDto.getChatId().toString());
        log.debug(chatSessionId.toString());
        throw new AppException("Invalid chat", HttpStatus.FORBIDDEN);
      }
      if (!messageDto.getOwnerId().equals(userId)) {
        throw new AppException("Invalid owner", HttpStatus.FORBIDDEN);
      }
      var message = service.create(messageDto, idempotencyKey);
      return ResponseEntity.created(URI.create(
          String.format("/v1/chat/%s/messages/%s", chatSessionId,
              message.getId().toString()))).body(message);
    } catch (UniqueViolation e) {
      log.warn(e.getMessage());
      return ResponseEntity.ok().build();
    }
  }


  @PutMapping("/{messageId}")
  public ResponseEntity<?> updateMessage(@PathVariable UUID chatSessionId,
      @PathVariable UUID messageId, @RequestBody MessageDto messageDto) {
    try {
      var updatedMessage = service.update(messageId, messageDto);
      log.debug("END UPDATING");
      return ResponseEntity.ok(updatedMessage);
    } catch (ResourceNotFound e) {
      log.warn(e.getMessage());
      throw new AppException(e.getMessage(), HttpStatus.NOT_FOUND);
    } catch (ConflictException e) {
      log.warn(e.getMessage());
      throw new AppException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }
}
