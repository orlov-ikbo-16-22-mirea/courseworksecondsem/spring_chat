package com.gitlab.uwa9k073.chat.dtos;

import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ChatSessionDto {

  private UUID firstPartnerId;
  private UUID secondPartnerId;
}
