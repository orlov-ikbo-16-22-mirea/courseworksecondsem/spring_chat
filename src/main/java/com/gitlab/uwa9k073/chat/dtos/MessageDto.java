package com.gitlab.uwa9k073.chat.dtos;

import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MessageDto {

  private UUID chatId;
  private UUID ownerId;
  private String payload;
}
