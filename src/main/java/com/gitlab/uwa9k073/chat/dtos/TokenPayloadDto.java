package com.gitlab.uwa9k073.chat.dtos;


import java.util.UUID;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TokenPayloadDto {

  private UUID userId;
  private UUID sessionId;
}
