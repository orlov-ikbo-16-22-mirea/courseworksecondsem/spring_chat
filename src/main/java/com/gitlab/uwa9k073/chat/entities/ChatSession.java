package com.gitlab.uwa9k073.chat.entities;


import com.datastax.oss.driver.api.mapper.annotations.NamingStrategy;
import com.datastax.oss.driver.api.mapper.entity.naming.NamingConvention;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.time.Instant;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.CassandraType;
import org.springframework.data.cassandra.core.mapping.CassandraType.Name;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

@Data
@Table(value = "chat_sessions")
@AllArgsConstructor
@NoArgsConstructor
@NamingStrategy(convention = NamingConvention.SNAKE_CASE_INSENSITIVE)
public class ChatSession {

  @PrimaryKeyColumn(type = PrimaryKeyType.PARTITIONED)
  private UUID id = UUID.randomUUID();
  @Column(value = "created_at")
  @CassandraType(type = Name.TIMESTAMP)
  private Instant createdAt = Instant.now();
  @Column(value = "first_partner_id")
  private UUID firstPartnerId;
  @Column(value = "second_partner_id")
  private UUID secondPartnerId;
  @JsonIgnore
  @Column(value = "idempotency_key")
  private UUID idempotencyKey;
}
