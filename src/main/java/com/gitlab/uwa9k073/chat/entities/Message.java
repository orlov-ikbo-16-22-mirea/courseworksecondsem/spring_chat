package com.gitlab.uwa9k073.chat.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.time.Instant;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(value = "messages")
public class Message {

  @PrimaryKeyColumn(type = PrimaryKeyType.CLUSTERED)
  private UUID id = UUID.randomUUID();
  @Column(value = "created_at")
  private Instant createdAt = Instant.now();
  @Column(value = "updated_at")
  private Instant updatedAt = Instant.now();
  @PrimaryKeyColumn(value = "chat_id", type = PrimaryKeyType.PARTITIONED)
  private UUID chatId;
  @Column(value = "owner_id")
  private UUID ownerId;
  @Column(value = "payload")
  private String payload;
  @JsonIgnore
  @Column(value = "idempotency_key")
  private UUID idempotencyKey;
}
