package com.gitlab.uwa9k073.chat.exceptions;

public class ConflictException extends RuntimeException {

  public ConflictException(String message) {
    super(message);
  }

}
