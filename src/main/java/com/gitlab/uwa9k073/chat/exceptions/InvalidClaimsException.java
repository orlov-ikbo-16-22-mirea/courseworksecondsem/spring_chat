package com.gitlab.uwa9k073.chat.exceptions;

public class InvalidClaimsException extends RuntimeException {

  public InvalidClaimsException(String message) {
    super(message);
  }

}
