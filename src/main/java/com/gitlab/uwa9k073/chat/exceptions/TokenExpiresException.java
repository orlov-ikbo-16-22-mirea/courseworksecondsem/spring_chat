package com.gitlab.uwa9k073.chat.exceptions;

public class TokenExpiresException extends RuntimeException {

  public TokenExpiresException(String message) {
    super(message);
  }
}
