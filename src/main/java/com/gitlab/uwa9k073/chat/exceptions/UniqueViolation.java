package com.gitlab.uwa9k073.chat.exceptions;

public class UniqueViolation extends RuntimeException {

  public UniqueViolation(String message) {
    super(message);
  }
}
