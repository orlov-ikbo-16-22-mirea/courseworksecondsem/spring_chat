package com.gitlab.uwa9k073.chat.repos;

import com.gitlab.uwa9k073.chat.entities.ChatSession;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.cassandra.repository.AllowFiltering;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.cassandra.repository.Query;

public interface ChatSessionRepo extends CassandraRepository<ChatSession, UUID> {

  @AllowFiltering
  List<ChatSession> findAllByFirstPartnerId(UUID userId);

  @AllowFiltering
  List<ChatSession> findAllBySecondPartnerId(UUID userId);

  @AllowFiltering
  boolean existsByIdempotencyKey(UUID idempotencyKey);

  @AllowFiltering
  Optional<ChatSession> getById(UUID chatId);

  @Query("DELETE FROM chat_sessions WHERE id = :chatId")
  void deleteSessionById(UUID chatId);
}
