package com.gitlab.uwa9k073.chat.repos;

import com.gitlab.uwa9k073.chat.entities.Message;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.cassandra.repository.AllowFiltering;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.cassandra.repository.Query;

public interface MessagesRepo extends CassandraRepository<Message, UUID> {

  @Query(value = "DELETE FROM messages WHERE chat_id = ?0 AND id IN ?1")
  void deleteAllByChatId(UUID chatId, List<UUID> ids);

  @Query(value = "SELECT * FROM messages WHERE chat_id = ?0 ALLOW FILTERING")
  List<Message> findAllByChatId(UUID chatId);

  @AllowFiltering
  boolean existsByIdempotencyKey(UUID idempotencyKey);

  @Query(value = "DELETE FROM messages WHERE chat_id = ?0 AND id = ?1")
  void deleteByChatIdAndOwnerIdAndId(UUID chatId, UUID messageId);

  @Query(value = "SELECT * FROM messages WHERE id = ?1 AND chat_id = ?0 ALLOW FILTERING")
  Optional<Message> takeByChatIdAndId(UUID chatId, UUID id);
}
