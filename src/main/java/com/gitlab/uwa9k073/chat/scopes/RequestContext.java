package com.gitlab.uwa9k073.chat.scopes;


import com.gitlab.uwa9k073.chat.dtos.TokenPayloadDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;

@RequestScope
@Component
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RequestContext {
private TokenPayloadDto tokenPayloadDto;
}
