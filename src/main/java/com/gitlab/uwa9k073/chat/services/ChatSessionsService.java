package com.gitlab.uwa9k073.chat.services;

import com.gitlab.uwa9k073.chat.dtos.ChatSessionDto;
import com.gitlab.uwa9k073.chat.entities.ChatSession;
import java.util.List;
import java.util.UUID;

public interface ChatSessionsService {

  ChatSession create(ChatSessionDto chatSessionDto, UUID idempotencyKey);

  List<ChatSession> getAll(UUID userId);

  ChatSession getById(UUID chatId);

  void deleteByChatId(UUID chatId);
}
