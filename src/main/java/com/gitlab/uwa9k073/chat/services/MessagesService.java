package com.gitlab.uwa9k073.chat.services;

import com.gitlab.uwa9k073.chat.dtos.MessageDto;
import com.gitlab.uwa9k073.chat.entities.Message;
import java.util.List;
import java.util.UUID;

public interface MessagesService {

  Message create(MessageDto messageDto, UUID idempotencyKey);

  List<Message> getAllInChat(UUID chatId);

  Message update(UUID messageId, MessageDto messageDto);

  void deleteMessage(UUID chatId, UUID userId, UUID messageId);

}
