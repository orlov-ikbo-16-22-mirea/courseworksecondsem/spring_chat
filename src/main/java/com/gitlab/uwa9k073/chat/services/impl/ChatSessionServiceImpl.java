package com.gitlab.uwa9k073.chat.services.impl;

import com.gitlab.uwa9k073.chat.dtos.ChatSessionDto;
import com.gitlab.uwa9k073.chat.entities.ChatSession;
import com.gitlab.uwa9k073.chat.entities.Message;
import com.gitlab.uwa9k073.chat.exceptions.ResourceNotFound;
import com.gitlab.uwa9k073.chat.exceptions.UniqueViolation;
import com.gitlab.uwa9k073.chat.repos.ChatSessionRepo;
import com.gitlab.uwa9k073.chat.repos.MessagesRepo;
import com.gitlab.uwa9k073.chat.services.ChatSessionsService;
import java.util.List;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
@Slf4j
public class ChatSessionServiceImpl implements ChatSessionsService {

  private ChatSessionRepo chatSessionRepo;
  private MessagesRepo messageRepo;

  private static final String CHAT_NOT_FOUND = "Chat not found";

  @Override
  public ChatSession create(ChatSessionDto chatSessionDto, UUID idempotencyKey) {
    var exists = chatSessionRepo.existsByIdempotencyKey(idempotencyKey);
    if (exists) {
      throw new UniqueViolation("Duplicate idempotency key");
    }
    var chatSession = new ChatSession();
    chatSession.setFirstPartnerId(chatSessionDto.getFirstPartnerId());
    chatSession.setSecondPartnerId(chatSessionDto.getSecondPartnerId());
    chatSession.setIdempotencyKey(idempotencyKey);
    var saved = chatSessionRepo.save(chatSession);
    log.debug(saved.toString());
    return saved;
  }

  @Override
  public List<ChatSession> getAll(UUID userId) {
    var chats = chatSessionRepo.findAllByFirstPartnerId(userId);
    if (chats.isEmpty()) {
      chats = chatSessionRepo.findAllBySecondPartnerId(userId);
      if (chats.isEmpty()) {
        throw new ResourceNotFound("User hasn't any chat");
      }
    }
    return chats;
  }

  @Override
  public ChatSession getById(UUID chatId) {
    log.debug("TRYING TO FIND CHAT");
    var optionalChat = chatSessionRepo.getById(chatId);
    if (optionalChat.isEmpty()) {
      throw new ResourceNotFound(CHAT_NOT_FOUND);
    }
    log.debug("CHAT SUCCESSFULLY FOUND");
    var chat = optionalChat.get();
    log.debug(chat.getCreatedAt().toString());
    log.debug(chat.toString());
    return chat;
  }

  @Override
  public void deleteByChatId(UUID chatId) {
    var exists = chatSessionRepo.getById(chatId);
    if (exists.isEmpty()) {
      throw new ResourceNotFound(CHAT_NOT_FOUND);
    }
    log.debug("STARING DELETE CHATS");
    chatSessionRepo.deleteSessionById(chatId);
    log.debug("FINDING MESSAGES FOR CHAT");
    var ids = messageRepo.findAllByChatId(chatId).stream().map(Message::getId).toList();
    log.debug("STARTING DELETE MESSAGES");
    messageRepo.deleteAllByChatId(chatId, ids);
  }
}
