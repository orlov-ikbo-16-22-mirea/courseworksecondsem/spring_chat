package com.gitlab.uwa9k073.chat.services.impl;

import com.gitlab.uwa9k073.chat.dtos.MessageDto;
import com.gitlab.uwa9k073.chat.entities.Message;
import com.gitlab.uwa9k073.chat.exceptions.ConflictException;
import com.gitlab.uwa9k073.chat.exceptions.ResourceNotFound;
import com.gitlab.uwa9k073.chat.exceptions.UniqueViolation;
import com.gitlab.uwa9k073.chat.repos.MessagesRepo;
import com.gitlab.uwa9k073.chat.services.MessagesService;
import java.time.Instant;
import java.util.List;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
@Slf4j
public class MessagesServiceImpl implements MessagesService {

  private final MessagesRepo messageRepo;

  private static final String MESSAGE_NOT_FOUND = "Message not found";

  @Override
  public Message create(MessageDto messageDto, UUID idempotencyKey) {
    var exists = messageRepo.existsByIdempotencyKey(idempotencyKey);
    if (exists) {
      throw new UniqueViolation("Duplicate idempotency key");
    }
    var message = new Message();
    message.setChatId(messageDto.getChatId());
    message.setOwnerId(messageDto.getOwnerId());
    message.setPayload(messageDto.getPayload());
    message.setIdempotencyKey(idempotencyKey);
    return messageRepo.save(message);
  }

  @Override
  public List<Message> getAllInChat(UUID chatId) {
    var messages = messageRepo.findAllByChatId(chatId);
    if (messages.isEmpty()) {
      throw new ResourceNotFound("No messages in chat");
    }
    return messages;
  }

  @Override
  public Message update(UUID messageId, MessageDto messageDto) {
    log.debug("START UPDATING");

    var optionalMessage = messageRepo.takeByChatIdAndId(messageDto.getChatId(), messageId);
    if (optionalMessage.isEmpty()) {
      throw new ResourceNotFound(MESSAGE_NOT_FOUND);
    }
    var message = optionalMessage.get();

    if (!message.getOwnerId().equals(messageDto.getOwnerId())) {
      throw new ConflictException("User is not owner of message");
    }

    message.setUpdatedAt(Instant.now());
    message.setPayload(messageDto.getPayload());

    log.debug(String.format("UPDATED MESSAGE: %s", message));
    var saved = messageRepo.save(message);
    log.debug(String.format("SAVED UPDATED MESSAGE: %s", saved));

    return messageRepo.save(message);
  }

  @Override
  public void deleteMessage(UUID chatId, UUID userId, UUID messageId) {
    log.debug("START DELETING");
    var exist = messageRepo.takeByChatIdAndId(chatId, messageId).isPresent();
    if (!exist) {
      throw new ResourceNotFound(MESSAGE_NOT_FOUND);
    }
    log.debug("ENTITY IS PRESENT");
    messageRepo.deleteByChatIdAndOwnerIdAndId(chatId, messageId);
  }
}
