package com.gitlab.uwa9k073.chat.controllers;


import com.gitlab.uwa9k073.chat.utils.ScyllaIntegrationTestBase;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;
import java.util.UUID;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class ChatSessionsControllerTest extends ScyllaIntegrationTestBase {

  private static final String CONCRETE_CHAT_SESSION_LOCATION_TEMPLATE = "/v1/chat/%s";
  private static final String FIRST_PARTNER_FIELD = "firstPartnerId";

  @Test
  void createChat() throws Exception {
    var requestBody = objectMapper.writeValueAsString(CHAT_SESSION_DTO);

    var response = mockMvc.perform(
            post(CREATE_CHAT_ENDPOINT).contentType(MediaType.APPLICATION_JSON).content(requestBody)
                .header(HttpHeaders.AUTHORIZATION, generateToken())
                .header(IDEMPOTENCY_KEY_HEADER_NAME, UUID.randomUUID()))
        .andExpectAll(status().isCreated()).andReturn().getResponse();

    var responseBody = objectMapper.readValue(response.getContentAsString(), HashMap.class);
    var userId = UUID.fromString(responseBody.get(FIRST_PARTNER_FIELD).toString());
    var partnerId = UUID.fromString(responseBody.get("secondPartnerId").toString());

    Assertions.assertEquals(USER_ID, userId);
    Assertions.assertEquals(PARTNER_ID, partnerId);
  }

  @Test
  void createChatWithExistingIdempotencyKey() throws Exception {
    var requestBody = objectMapper.writeValueAsString(CHAT_SESSION_DTO);
    var idempotencyKey = UUID.randomUUID();

    mockMvc.perform(
            post(CREATE_CHAT_ENDPOINT).contentType(MediaType.APPLICATION_JSON).content(requestBody)
                .header(HttpHeaders.AUTHORIZATION, generateToken())
                .header(IDEMPOTENCY_KEY_HEADER_NAME, idempotencyKey))
        .andExpectAll(status().isCreated()).andReturn().getResponse();

    var response = mockMvc.perform(
            post(CREATE_CHAT_ENDPOINT).contentType(MediaType.APPLICATION_JSON).content(requestBody)
                .header(HttpHeaders.AUTHORIZATION, generateToken())
                .header(IDEMPOTENCY_KEY_HEADER_NAME, idempotencyKey))
        .andExpectAll(status().isOk()).andReturn().getResponse();
  }

  @Test
  void getUserChatSessions() throws Exception {
    var requestBody = objectMapper.writeValueAsString(CHAT_SESSION_DTO);

    mockMvc.perform(
            post(CREATE_CHAT_ENDPOINT).contentType(MediaType.APPLICATION_JSON).content(requestBody)
                .header(HttpHeaders.AUTHORIZATION, generateToken())
                .header(IDEMPOTENCY_KEY_HEADER_NAME, UUID.randomUUID()))
        .andExpectAll(status().isCreated()).andReturn().getResponse();

    var response = mockMvc.perform(
            get(USER_CHAT_ENDPOINT).header(HttpHeaders.AUTHORIZATION, generateToken()))
        .andExpectAll(status().isOk()).andReturn()
        .getResponse();

    ArrayList<?> responseBody = objectMapper.readValue(response.getContentAsString(),
        ArrayList.class);
    Assertions.assertEquals(USER_ID, UUID.fromString(
        ((HashMap<?, ?>) responseBody.getFirst()).get(FIRST_PARTNER_FIELD).toString()));

  }

  @Test
  void getUserChatSessionWithNoChatSession() throws Exception {
    mockMvc.perform(get(USER_CHAT_ENDPOINT).header(HttpHeaders.AUTHORIZATION, generateToken()))
        .andExpectAll(status().isNotFound());
  }

  @Test
  void getChatSession() throws Exception {
    var requestBody = objectMapper.writeValueAsString(CHAT_SESSION_DTO);

    var response = mockMvc.perform(
            post(CREATE_CHAT_ENDPOINT).contentType(MediaType.APPLICATION_JSON).content(requestBody)
                .header(HttpHeaders.AUTHORIZATION, generateToken())
                .header(IDEMPOTENCY_KEY_HEADER_NAME, UUID.randomUUID()))
        .andExpectAll(status().isCreated(), header().exists(HttpHeaders.LOCATION)).andReturn()
        .getResponse();
    var location = response.getHeader(HttpHeaders.LOCATION);
    var session = objectMapper.readValue(response.getContentAsString(), HashMap.class);

    response = mockMvc.perform(get(location).header(HttpHeaders.AUTHORIZATION, generateToken()))
        .andReturn().getResponse();

    var responseBody = objectMapper.readValue(response.getContentAsString(), HashMap.class);
    compareChatFields(session, responseBody);
  }

  @Test
  void getSessionWithNoChatSession() throws Exception {
    var location = String.format(CONCRETE_CHAT_SESSION_LOCATION_TEMPLATE, USER_ID.toString());
    mockMvc.perform(get(location).header(HttpHeaders.AUTHORIZATION, generateToken()))
        .andExpectAll(status().isNotFound());
  }

  @Test
  void deleteChatSession() throws Exception {
    var requestBody = objectMapper.writeValueAsString(CHAT_SESSION_DTO);

    var response = mockMvc.perform(
            post(CREATE_CHAT_ENDPOINT).contentType(MediaType.APPLICATION_JSON).content(requestBody)
                .header(HttpHeaders.AUTHORIZATION, generateToken())
                .header(IDEMPOTENCY_KEY_HEADER_NAME, UUID.randomUUID()))
        .andExpectAll(status().isCreated()).andReturn().getResponse();
    var session = objectMapper.readValue(response.getContentAsString(), HashMap.class);

    var location = response.getHeader(HttpHeaders.LOCATION);

    mockMvc.perform(delete(location).header(HttpHeaders.AUTHORIZATION, generateToken()))
        .andExpectAll(status().isNoContent()).andReturn()
        .getResponse();

    Assertions.assertEquals(Optional.empty(),
        chatSessionRepo.getById(UUID.fromString(session.get("id").toString())));
  }

  @Test
  void deleteWithNoChatSession() throws Exception {
    var location = String.format(CONCRETE_CHAT_SESSION_LOCATION_TEMPLATE, USER_ID.toString());
    mockMvc.perform(delete(location).header(HttpHeaders.AUTHORIZATION, generateToken()))
        .andExpectAll(status().isNotFound());
  }
}
