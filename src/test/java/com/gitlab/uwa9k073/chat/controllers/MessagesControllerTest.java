package com.gitlab.uwa9k073.chat.controllers;

import com.gitlab.uwa9k073.chat.entities.ChatSession;
import com.gitlab.uwa9k073.chat.services.ChatSessionsService;
import com.gitlab.uwa9k073.chat.services.MessagesService;
import com.gitlab.uwa9k073.chat.utils.ScyllaIntegrationTestBase;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;
import java.util.UUID;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class MessagesControllerTest extends ScyllaIntegrationTestBase {

  @Autowired
  private ChatSessionsService sessionsService;
  @Autowired
  private MessagesService messagesService;

  private ChatSession chatSession;

  private static final String CHAT_FIELD = "chatId";
  private static final String OWNER_FIELD = "ownerId";
  private static final String PAYLOAD_FIELD = "payload";

  @BeforeEach
  void createSession() {
    chatSession = sessionsService.create(CHAT_SESSION_DTO, UUID.randomUUID());
  }

  @Test
  void getAllMessages() throws Exception {
    MESSAGE_DTO.setChatId(chatSession.getId());
    messagesService.create(MESSAGE_DTO, UUID.randomUUID());

    var response = mockMvc.perform(
            get(String.format(CREATE_MESSAGE_ENDPOINT_TEMPLATE, chatSession.getId().toString())
                + "/fill").header(HttpHeaders.AUTHORIZATION, generateToken()))
        .andExpectAll(status().isOk()).andReturn().getResponse();

    var responseBody = objectMapper.readValue(response.getContentAsString(), ArrayList.class);
    var firstMessage = (HashMap<?, ?>) responseBody.getFirst();

    Assertions.assertEquals(MESSAGE_DTO.getChatId().toString(),
        firstMessage.get(CHAT_FIELD).toString());
    Assertions.assertEquals(MESSAGE_DTO.getOwnerId().toString(),
        firstMessage.get(OWNER_FIELD).toString());
    Assertions.assertEquals(MESSAGE_DTO.getPayload(), firstMessage.get(PAYLOAD_FIELD).toString());
  }


  @Test
  void deleteMessage() throws Exception {
    MESSAGE_DTO.setChatId(chatSession.getId());
    var message = messagesService.create(MESSAGE_DTO, UUID.randomUUID());

    mockMvc.perform(
            delete(String.format(CREATE_MESSAGE_ENDPOINT_TEMPLATE, chatSession.getId().toString())
                + String.format("/%s", message.getId())).header(HttpHeaders.AUTHORIZATION,
                generateToken()))
        .andExpectAll(status().isNoContent()).andReturn().getResponse();

    Assertions.assertEquals(Optional.empty(),
        messagesRepo.takeByChatIdAndId(chatSession.getId(), message.getId()));
  }

  @Test
  void createMessage() throws Exception {
    MESSAGE_DTO.setChatId(chatSession.getId());
    String createMessageEndpoint = String.format(CREATE_MESSAGE_ENDPOINT_TEMPLATE,
        chatSession.getId().toString());
    var requestBody = objectMapper.writeValueAsString(MESSAGE_DTO);
    var response = mockMvc.perform(
            post(createMessageEndpoint).header(HttpHeaders.AUTHORIZATION, generateToken())
                .header(IDEMPOTENCY_KEY_HEADER_NAME, UUID.randomUUID()).contentType(MediaType.APPLICATION_JSON)
                .content(requestBody))
        .andExpectAll(status().isCreated(), header().exists(HttpHeaders.LOCATION)).andReturn()
        .getResponse();
    var responseBody = objectMapper.readValue(response.getContentAsString(), HashMap.class);

    Assertions.assertEquals(MESSAGE_DTO.getChatId().toString(),
        responseBody.get(CHAT_FIELD).toString());
    Assertions.assertEquals(MESSAGE_DTO.getOwnerId().toString(),
        responseBody.get(OWNER_FIELD).toString());
    Assertions.assertEquals(MESSAGE_DTO.getPayload(), responseBody.get(PAYLOAD_FIELD).toString());

  }

  @Test
  void updateMessage() throws Exception {
    MESSAGE_DTO.setChatId(chatSession.getId());
    var message = messagesService.create(MESSAGE_DTO, UUID.randomUUID());
    MESSAGE_DTO.setPayload("ANOTHER HI");

    var response = mockMvc.perform(
            put(String.format("/v1/chat/%s/messages/%s", message.getChatId(), message.getId())).header(
                    HttpHeaders.AUTHORIZATION, generateToken()).contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(MESSAGE_DTO)))
        .andExpectAll(status().isOk(), content().contentType(MediaType.APPLICATION_JSON))
        .andReturn().getResponse();

    var responseBody = objectMapper.readValue(response.getContentAsString(), HashMap.class);

    Assertions.assertEquals(message.getId().toString(), responseBody.get("id").toString());
    Assertions.assertEquals(message.getChatId().toString(), responseBody.get(CHAT_FIELD).toString());
    Assertions.assertEquals(message.getOwnerId().toString(),
        responseBody.get(OWNER_FIELD).toString());
    Assertions.assertEquals(MESSAGE_DTO.getPayload(), responseBody.get(PAYLOAD_FIELD).toString());
    Assertions.assertNotEquals(message.getUpdatedAt().toString(),
        responseBody.get("updatedAt").toString());
  }
}
