package com.gitlab.uwa9k073.chat.utils;

//import com.gitlab.uwa9k073.chat.Application;
//import org.springframework.boot.SpringApplication;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.testcontainers.service.connection.ServiceConnection;
import org.springframework.context.annotation.Bean;
import org.testcontainers.containers.CassandraContainer;
import org.testcontainers.utility.DockerImageName;

@TestConfiguration(proxyBeanMethods = false)
public class ConfTest {

  private static final Integer CONTAINER_PORT = 9042;
  private static final String KEYSPACE = "chat";
  private static final String INIT_SCRIPT_PATH = "db_1.cql";

  private static final DockerImageName CASSANDRA_IMAGE = DockerImageName.parse(
      "cassandra:latest");
  private static final DockerImageName SCYLLA_IMAGE = DockerImageName.parse(
      "scylladb/scylla:latest").asCompatibleSubstituteFor(CASSANDRA_IMAGE);

  @Bean
  @ServiceConnection
  CassandraContainer<?> cassandraContainer() {
    var container = new CassandraContainer<>(SCYLLA_IMAGE).withInitScript(INIT_SCRIPT_PATH)
        .withExposedPorts(CONTAINER_PORT);
    container.start();
    System.setProperty("spring.cassandra.key-space", KEYSPACE);
    System.setProperty("spring.cassandra.contact-points",
        container.getContactPoint().getHostName() + ":" + container.getMappedPort(CONTAINER_PORT).toString());
    System.setProperty("spring.cassandra.local-datacenter", container.getLocalDatacenter());
    System.setProperty("spring.cassandra.port", container.getMappedPort(CONTAINER_PORT).toString());
    return container;
  }

//  public static void main(String[] args) {
//    SpringApplication.from(Application::main).with(ConfTest.class).run(args);
//  }
}
