package com.gitlab.uwa9k073.chat.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.uwa9k073.chat.config.UserAuthenticationProvider;
import com.gitlab.uwa9k073.chat.dtos.ChatSessionDto;
import com.gitlab.uwa9k073.chat.dtos.MessageDto;
import com.gitlab.uwa9k073.chat.repos.ChatSessionRepo;
import com.gitlab.uwa9k073.chat.repos.MessagesRepo;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest(classes = ConfTest.class)
@AutoConfigureMockMvc
public class ScyllaIntegrationTestBase {

  protected static final List<String> CHAT_COMPARING_FIELDS = List.of("id", "firstPartnerId",
      "secondPartnerId");

  protected static void compareChatFields(Map<?, ?> lhs, Map<?, ?> rhs) {
    for (var field : CHAT_COMPARING_FIELDS) {
      Assertions.assertEquals(lhs.get(field), rhs.get(field));
    }
  }

  protected static final String CREATE_CHAT_ENDPOINT = "/v1/chat";
  protected static final String USER_CHAT_ENDPOINT = "/v1/chat/fill";

  protected static final String CREATE_MESSAGE_ENDPOINT_TEMPLATE = "/v1/chat/%s/messages";

  protected final ObjectMapper objectMapper = new ObjectMapper();

  protected static final UUID USER_ID = UUID.randomUUID();
  protected static final UUID PARTNER_ID = UUID.randomUUID();

  @Autowired
  protected MockMvc mockMvc;

  @Autowired
  private UserAuthenticationProvider provider;

  @Autowired
  protected MessagesRepo messagesRepo;
  @Autowired
  protected ChatSessionRepo chatSessionRepo;

  protected static final String IDEMPOTENCY_KEY_HEADER_NAME = "X-Idempotency-Key";

  protected String generateToken() {
    var token = provider.createToken(USER_ID, UUID.randomUUID()).getToken();
    return String.format("Bearer %s", token);
  }

  @AfterEach
  public void cleanupDB() {
    chatSessionRepo.deleteAll();
    messagesRepo.deleteAll();
  }

  protected static final ChatSessionDto CHAT_SESSION_DTO = ChatSessionDto.builder()
      .firstPartnerId(USER_ID).secondPartnerId(PARTNER_ID).build();
  protected static final MessageDto MESSAGE_DTO = MessageDto.builder().ownerId(USER_ID)
      .payload("HI").build();
}
